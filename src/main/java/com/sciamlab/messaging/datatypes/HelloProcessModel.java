package com.sciamlab.messaging.datatypes;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.swing.text.DateFormatter;

public class HelloProcessModel {

	private String firstname;
	private String lastname;
	private String email;
	private Integer age;
	private Date birthdate;
	
	
	public HelloProcessModel() {	
	}
	
	
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) throws ParseException {
		this.birthdate = birthdate;
	}
	
	public Map<String,Object> toMap() {
		HashMap<String,Object> hm = new HashMap<String,Object>();
		hm.put("firstname", this.firstname);
		hm.put("lastname",this.lastname);
		hm.put("email",this.email);
		hm.put("age",this.age);
		hm.put("birdate",this.birthdate);
		return hm;
	}

	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer sb = new StringBuffer("");
		sb.append(this.firstname + " " + this.lastname + "\n");
		sb.append(this.email + "\n");
		sb.append(this.birthdate + "\n");
		sb.append(this.age + "\n");
		
		return sb.toString();
	}
}
