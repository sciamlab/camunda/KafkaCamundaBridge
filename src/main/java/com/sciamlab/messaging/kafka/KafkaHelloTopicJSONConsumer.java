package com.sciamlab.messaging.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sciamlab.messaging.datatypes.HelloProcessModel;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.connect.json.JsonSerializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Properties;
import java.util.Scanner;
import java.util.TimeZone;

public class KafkaHelloTopicJSONConsumer {
    private static Scanner in;
    
	public static void main(String[] args) throws Exception {

		System.err.printf("Usage: %s <topicName> [groupId]\n\nDefault to HelloTopic HPConsumerGroup\n",com.sciamlab.messaging.kafka.KafkaHelloTopicJSONConsumer.class.getSimpleName());

		String topicName = "HelloTopic";
        String groupId = "HPConsumerGroup";
        
        in = new Scanner(System.in);
        
        if (args.length == 1 ) {
            topicName = args[0];
        } else if (args.length == 2 ) {
            topicName = args[0];
        	groupId = args[1];
        }
        
        ConsumerThread consumerRunnable = new ConsumerThread(topicName, groupId);
        consumerRunnable.start();
        String line = "";
        while (!line.equals("exit")) {
        	line = in.next();
        }
        
        consumerRunnable.getKafkaConsumer().wakeup();
        System.out.println("Stopping consumer .....");
        consumerRunnable.join();
	}
	
	private static class ConsumerThread extends Thread {
		private String topicName;
		private String groupId;	
		private KafkaConsumer<String,JsonNode> kafkaConsumer;
		
		public static final MediaType JSON = 
				MediaType.parse("application/json; charset=utf-8");
		
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		OkHttpClient httpClient = new OkHttpClient();

		String prepareJSON(String bizKey, HelloProcessModel hpm) {
		    return "{" + 
		    		"  \"businessKey\" : \""+bizKey+"\"," + 
		    		"  \"variables\": {" + 
		    		"    \"firstname\": {\"value\" : \""+hpm.getFirstname()+"\",\"type\":\"String\"}," + 
		    		"    \"lastname\": {\"value\" : \""+hpm.getLastname()+"\",\"type\":\"String\"}," + 
		    		"    \"email\": {\"value\" : \""+hpm.getEmail()+"\",\"type\":\"String\"}," + 
		    		"    \"age\": {\"value\" : "+hpm.getAge()+",\"type\":\"Long\"}," + 
		    		"    \"birthdate\": {\"value\" : \""+df.format(hpm.getBirthdate())+"\",\"type\":\"String\"}" + 
		    		"  }\n" + 
		    		"}";
		}

		
        String postJSONData(String url, String json) throws IOException {
      	  RequestBody body = RequestBody.create(JSON, json);
      	  Request request = new Request.Builder()
      	      .url(url)
      	      .post(body)
      	      .build();
      	  Response response = httpClient.newCall(request).execute();
      	  return response.body().string();
        }
        
		public ConsumerThread(String topicName, String groupId) {
			this.topicName = topicName;
			this.groupId = groupId;
		}
		
		public void run( ) {

            Properties configProperties = new Properties();
            configProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
            configProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
            configProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.connect.json.JsonDeserializer");
            configProperties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
            configProperties.put(ConsumerConfig.CLIENT_ID_CONFIG, "HPConsumer");


            //Figure out where to start processing messages from
            kafkaConsumer = new KafkaConsumer<String, JsonNode>(configProperties);
            kafkaConsumer.subscribe(Arrays.asList(topicName));
            ObjectMapper mapper = new ObjectMapper();


            
            //Start processing messages
            try {
                while (true) {
                    ConsumerRecords<String, JsonNode> records = kafkaConsumer.poll(100);
                    for (ConsumerRecord<String, JsonNode> record : records) {
                        JsonNode jsonNode = record.value();
                        HelloProcessModel customerData = mapper.treeToValue(jsonNode,HelloProcessModel.class);
                        String jsonBody = prepareJSON(record.topic()+"#"+record.partition()+"#"+record.offset(), customerData);

                        System.out.println(customerData);
                        System.out.println(jsonBody);
                        
                        // connect to camunda using okHttpClient
                        // and create an instance using the submit-form REST API
                        // http://localhost:8080/engine-rest/process-definition/key/hello-process/submit-form
                        String response = postJSONData("http://localhost:8080/engine-rest/process-definition/key/hello-process/submit-form", jsonBody);
                        System.out.println(response);
                    }
                }                
            }catch(WakeupException ex){
                System.out.println("Exception caught " + ex.getMessage());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } finally{
                kafkaConsumer.close();
                System.out.println("After closing KafkaConsumer");
            }
        }

		public KafkaConsumer<String,JsonNode> getKafkaConsumer(){
            return this.kafkaConsumer;
        }
	}
}
