package com.sciamlab.messaging.kafka;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.sciamlab.messaging.datatypes.HelloProcessModel;

public class ObjectMapperTest {

	public static void main(String[] args) throws ParseException, IOException {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		df.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		
		HelloProcessModel hpm = new HelloProcessModel();
		hpm.setFirstname("Alessio");
		hpm.setLastname("Dragoni");
		hpm.setEmail("ad@sciamlab.com");
		hpm.setAge(49);
		hpm.setBirthdate(df.parse("1969-06-16 08:00"));

        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.setDateFormat(new ISO8601DateFormat());

        
        HelloProcessModel parsedHpm = mapper.readValue("{\"firstname\":\"Alessio\",\"lastname\":\"Dragoni\",\"email\":\"ad@sciamlab.com\",\"age\":49,\"birthdate\":\"1969-06-16T06:00:00Z\"}", HelloProcessModel.class);
        
        System.out.println("Object 2 JSON: " + mapper.writeValueAsString(hpm) );
        System.out.println("JSON 2 Object: " + parsedHpm.toString());
	}

}
