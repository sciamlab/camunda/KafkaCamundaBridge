package com.sciamlab.messaging.kafka;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sciamlab.messaging.datatypes.HelloProcessModel;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.connect.json.JsonSerializer;

import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.Future;

public class KafkaHelloTopicJSONProducer {
    private static Scanner in;
    
	public static void main(String[] args) throws Exception {

		System.err.printf("Usage: %s <topicName>\n\nDefault to HelloTopic\n",com.sciamlab.messaging.kafka.KafkaHelloTopicJSONProducer.class.getSimpleName());
		
        String topicName = "HelloTopic";        
        
        in = new Scanner(System.in);
        
        if (args.length == 1 ) {
            topicName = args[0];
        }
        
        System.out.println("Enter message(type exit to quit)");

        //Configure the Producer
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:9092");
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.ByteArraySerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.connect.json.JsonSerializer");

        org.apache.kafka.clients.producer.Producer producer = new KafkaProducer(configProperties);

        ObjectMapper objectMapper = new ObjectMapper();

        String line = in.nextLine();
        while(!line.equals("exit")) {
        	HelloProcessModel hellodata = objectMapper.readValue(line, HelloProcessModel.class);
            JsonNode  jsonNode = objectMapper.valueToTree(hellodata);
            ProducerRecord<String, JsonNode> rec = new ProducerRecord<String, JsonNode>(topicName,jsonNode);
            Future f = producer.send(rec);
            System.out.println(f.get().toString());
            
            
            line = in.nextLine();
        }
        in.close();
        producer.close();
	}

}
