package com.sciamlab.bpm;

import java.util.HashMap;
import java.util.List;

import org.camunda.bpm.application.PostDeploy;
import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.EmbeddedProcessApplication;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.runtime.ProcessInstance;

import com.sciamlab.messaging.datatypes.HelloProcessModel;

@ProcessApplication
public class CamundaHelloProcessApplication extends EmbeddedProcessApplication {

	ProcessEngine pe;
	String name;
	
	public CamundaHelloProcessApplication(String engineName) {
		this.name = engineName;
	}
	
	@PostDeploy
	public void onDeploymentFinished(ProcessEngine processEngine) {
		System.out.println("Camunda BPM Engine started!");
		System.out.println("Process Engines: "+ProcessEngines.getProcessEngines());
		this.pe = ProcessEngines.getProcessEngine(this.name);
		System.out.println("Process Engine caricato: "+pe.getName());
	}
	/*
	 * This method return the list of running process instances on HelloProcess Process Template
	 */
	public List<ProcessInstance> listProcessInstance() {
		return this.pe.getRuntimeService().createProcessInstanceQuery()
			.processDefinitionKey("hello-process")
			.list();
	}
	
	
	/*
	 * This method fireup a new process instance on the HelloProcess template
	 * and set the initial ProcessVariable according to the given HelloProcessModel
	 */
	public ProcessInstance fireInstance(String bizKey, HelloProcessModel hpm) {
		return this.pe.getRuntimeService().startProcessInstanceByKey("hello-process", bizKey, hpm.toMap());
	}
	
}
