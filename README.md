# KafkaSampleJSONMessage

This little example contains a data model for an example message and a Kafka JSON Message Producer and Consumer.
The JSONMessageConsumer act as well as a bridge to Camunda BPM

## Sample data to use with message producer
```
{"firstname":"Alessio","lastname":"Dragoni","email":"ad@sciamlab.com","age":49,"birthdate":"1969-06-16T06:00:00Z"}
{"firstname":"Marcello","lastname":"Riccetti","email":"mricetti@gmail.com","age":40,"birthdate":"1978-12-16T06:00:00Z"}
{"firstname":"Antonio","lastname":"Colli","email":"ancolli@outlook.com","age":31,"birthdate":"1987-02-11T06:00:00Z"}
```

## Dependencies
this assume your process engine is running connected to a database server, in case you are using H2 this must be started as server listening on a TCP port. It also assume you already have deployed on your engine the [HelloProcess application](https://gitlab.com/sciamlab/camunda/hello-process) 

## License
Please see [LICENSE](LICENSE) for more information